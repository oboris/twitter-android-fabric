package ua.edu.cdu.boris.simplemobiletwitterfabricclient.services;


import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.models.User;

import retrofit.http.GET;
import retrofit.http.Query;
import ua.edu.cdu.boris.simplemobiletwitterfabricclient.model.UserIds;

public interface CustomService {
    @GET("/1.1/users/show.json")
    void showUser(@Query("user_id") long id, Callback<User> cb);

    @GET("/1.1/followers/ids.json")
    void listFollowers(@Query("user_id") long id, Callback<UserIds> cb);

    @GET("/1.1/friends/ids.json")
    void listFriends(@Query("user_id") long id, Callback<UserIds> cb);
}
