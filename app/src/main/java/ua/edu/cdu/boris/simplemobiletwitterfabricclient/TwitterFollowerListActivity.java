package ua.edu.cdu.boris.simplemobiletwitterfabricclient;

import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.widget.TextView;

import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.models.User;

import java.util.List;

import ua.edu.cdu.boris.simplemobiletwitterfabricclient.dataBase.DBHelper;
import ua.edu.cdu.boris.simplemobiletwitterfabricclient.model.SimpleUser;
import ua.edu.cdu.boris.simplemobiletwitterfabricclient.model.UserIds;
import ua.edu.cdu.boris.simplemobiletwitterfabricclient.services.MyTwitterApiClient;
import ua.edu.cdu.boris.simplemobiletwitterfabricclient.services.TwitterUserCursorAdapter;

public class TwitterFollowerListActivity extends ListActivity {

    private TwitterUserCursorAdapter twitterUserCursorAdapter;
    Cursor cursor;
    ProgressDialog progressDialog;

    private long twUserId;

    private MyTwitterApiClient myTwitterApiClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_twitter_follower_list);

        Intent intent = getIntent();
        int typeBtn = intent.getIntExtra(TwitterUserListActivity.EXTRA_MESSAGE_TYPE, 1);

        TextView tvCaption = (TextView) findViewById(R.id.tvCaption);
        switch (typeBtn) {
            case 1:
                tvCaption.setText(R.string.tvFollowers);
                break;
            case 2:
                tvCaption.setText(R.string.tvFriends);
                break;
        }

        TwitterSession session = Twitter.getSessionManager().getActiveSession();
        myTwitterApiClient = new MyTwitterApiClient(session);
        twUserId = session.getUserId();

        clearUsersDataBase();
        loadUserList(typeBtn);

        twitterUserCursorAdapter = new TwitterUserCursorAdapter(this, null, 0);
        setListAdapter(twitterUserCursorAdapter);
    }

    private void loadUserList(int typeBtn) {
        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Loading");
        progressDialog.setMessage("Wait until tweets loading...");
        progressDialog.show();

        switch (typeBtn) {
            case 1:
                myTwitterApiClient.getCustomService().listFollowers(twUserId, loadUsersCallback);
                break;
            case 2:
                myTwitterApiClient.getCustomService().listFriends(twUserId, loadUsersCallback);
                break;
        }
    }

    private Callback<UserIds> loadUsersCallback = new Callback<UserIds>() {
        @Override
        public void success(Result<UserIds> result) {
            storeUsersInDatabase(result.data.getIds());
            progressDialog.dismiss();
        }

        @Override
        public void failure(TwitterException e) {
            progressDialog.dismiss();
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(TwitterFollowerListActivity.this);
            dialogBuilder.setTitle("Error");
            dialogBuilder.setMessage(e.getMessage());
            dialogBuilder.create().show();
        }
    };

    private void storeUsersInDatabase(List<Long> listUsersId) {
        for (Long userId : listUsersId) {
            myTwitterApiClient.getCustomService().showUser(userId, new Callback<User>() {
                @Override
                public void success(Result<User> result) {
                    SimpleUser simpleUser = new SimpleUser(result.data.id, result.data.name, String.format("@%s", result.data.screenName), result.data.profileImageUrl);
                    DBHelper.getInstance(TwitterFollowerListActivity.this).insertSimpleUsers(simpleUser);
                    updateAdapterCursor();
                }

                @Override
                public void failure(TwitterException exception) {
                    exception.printStackTrace();
                }
            });
        }
    }

    private void updateAdapterCursor() {
        cursor = DBHelper.getInstance(this).getAllUsers();
        if (cursor != null) {
            cursor.moveToFirst();
            twitterUserCursorAdapter.swapCursor(cursor);
            twitterUserCursorAdapter.notifyDataSetChanged();
        }
    }

    private void clearUsersDataBase() {
        DBHelper.getInstance(this).deleteAllSimpleUsers();
    }
}
