package ua.edu.cdu.boris.simplemobiletwitterfabricclient.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UserIds {
    @SerializedName("ids")
    public List<Long> ids;

    public UserIds(List<Long> ids) {
        this.ids = ids;
    }

    public List<Long> getIds() {
        return ids;
    }
}
