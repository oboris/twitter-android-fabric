package ua.edu.cdu.boris.simplemobiletwitterfabricclient;

import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterApiClient;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.models.Tweet;
import com.twitter.sdk.android.core.models.User;
import com.twitter.sdk.android.core.services.StatusesService;
import com.twitter.sdk.android.tweetcomposer.ComposerActivity;

import java.util.List;

import ua.edu.cdu.boris.simplemobiletwitterfabricclient.dataBase.DBHelper;
import ua.edu.cdu.boris.simplemobiletwitterfabricclient.model.SimpleTweet;
import ua.edu.cdu.boris.simplemobiletwitterfabricclient.services.MyTwitterApiClient;
import ua.edu.cdu.boris.simplemobiletwitterfabricclient.services.TwitterCursorAdapter;

public class TwitterUserListActivity extends ListActivity {

    public final static String EXTRA_MESSAGE_ID = "ua.edu.cdu.boris.simplemobiletwitterfabricclient.MESSAGEID";
    public final static String EXTRA_MESSAGE_TYPE = "ua.edu.cdu.boris.simplemobiletwitterfabricclient.MESSAGETYPE";

    private TwitterCursorAdapter twitterCursorAdapter;
    private ProgressDialog progressDialog;
    private boolean successTweetsRead = false;

    private long twUserId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_twitter_user_list);

        Button twSentTweet = (Button) findViewById(R.id.twSendTweet);
        Button twLogoutButton = (Button) findViewById(R.id.twLogout);
        Button twRefreshButton = (Button) findViewById(R.id.twRefresh);
        Button twShowFollowersButton = (Button) findViewById(R.id.twShowFollowers);
        Button twShowFriendsButton = (Button) findViewById(R.id.twShowFriends);

        twSentTweet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                publishTweet();
            }
        });

        twLogoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Twitter.getSessionManager().clearActiveSession();
                finish();
            }
        });

        twRefreshButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadUserTweets(1);
            }
        });

        twShowFollowersButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.putExtra(EXTRA_MESSAGE_TYPE, 1);
                intent.setClass(TwitterUserListActivity.this, TwitterFollowerListActivity.class);
                startActivity(intent);
            }
        });

        twShowFriendsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.putExtra(EXTRA_MESSAGE_TYPE, 2);
                intent.setClass(TwitterUserListActivity.this, TwitterFollowerListActivity.class);
                startActivity(intent);
            }
        });

        final TextView tvUserFullName = (TextView) findViewById(R.id.tvUserFullName);
        final TextView tvUserName = (TextView) findViewById(R.id.tvUserName);
        final ImageView ivUserImage = (ImageView) findViewById(R.id.imageView);

        TwitterSession session = Twitter.getSessionManager().getActiveSession();
        MyTwitterApiClient myTwitterApiClient = new MyTwitterApiClient(session);
        twUserId = session.getUserId();
        myTwitterApiClient.getCustomService().showUser(twUserId, new Callback<User>() {
            @Override
            public void success(Result<User> result) {
                tvUserFullName.setText(result.data.name);
                tvUserName.setText(String.format("@%s", result.data.screenName));
                Picasso.with(TwitterUserListActivity.this).load(result.data.profileImageUrl).into(ivUserImage);
            }

            @Override
            public void failure(TwitterException exception) {
                exception.printStackTrace();
            }
        });

        loadUserTweets(null);

        twitterCursorAdapter = new TwitterCursorAdapter(this, null, 0);
        setListAdapter(twitterCursorAdapter);
    }

    private void loadUserTweets(Integer twNum){
        TwitterApiClient twitterApiClient = TwitterCore.getInstance().getApiClient();
        StatusesService statusesService = twitterApiClient.getStatusesService();

        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Loading");
        progressDialog.setMessage("Wait until tweets loading...");
        progressDialog.show();

        if (twNum == null || !successTweetsRead) {
            statusesService.userTimeline(twUserId, null, null, null, null, null, null, null, null, loadAllTweetsCallback);
        } else {
            statusesService.userTimeline(twUserId, null, twNum, null, null, null, null, null, null, loadTweetsCallback);
        }
    }

    private Callback<List<Tweet>> loadAllTweetsCallback = new Callback<List<Tweet>>() {
        @Override
        public void success(Result<List<Tweet>> result) {
            successTweetsRead = true;
            clearTweetsDataBase();
            storeTweetsInDatabase(result.data);
            updateAdapterCursor();
            progressDialog.dismiss();
        }

        @Override
        public void failure(TwitterException e) {
            progressDialog.dismiss();
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(TwitterUserListActivity.this);
            dialogBuilder.setTitle("Error");
            dialogBuilder.setMessage(e.getMessage());
            dialogBuilder.create().show();
            updateAdapterCursor();
        }
    };

    private Callback<List<Tweet>> loadTweetsCallback = new Callback<List<Tweet>>() {
        @Override
        public void success(Result<List<Tweet>> result) {
            storeTweetsInDatabase(result.data);
            updateAdapterCursor();
            progressDialog.dismiss();
        }

        @Override
        public void failure(TwitterException e) {
            progressDialog.dismiss();
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(TwitterUserListActivity.this);
            dialogBuilder.setTitle("Error");
            dialogBuilder.setMessage(e.getMessage());
            dialogBuilder.create().show();
        }
    };

    private void clearTweetsDataBase(){
        DBHelper.getInstance(this).deleteAllSimpleTweets();
    }

    private void storeTweetsInDatabase(List<Tweet> tweets) {
        for (Tweet tweet : tweets) {
            SimpleTweet simpleTweet = new SimpleTweet(tweet.id, tweet.createdAt, tweet.text);
            DBHelper.getInstance(this).insertSimpleTweet(simpleTweet);
        }
    }

    private void updateAdapterCursor() {
        Cursor cursor = DBHelper.getInstance(this).getAllTweets();
        if (cursor != null) {
            cursor.moveToFirst();
            twitterCursorAdapter.swapCursor(cursor);
            twitterCursorAdapter.notifyDataSetChanged();
        }
    }

    private void publishTweet() {
        final TwitterSession session = TwitterCore.getInstance().getSessionManager().getActiveSession();
        final Intent intent = new ComposerActivity.Builder(TwitterUserListActivity.this)
                .session(session)
                .createIntent();
        startActivityForResult(intent, 1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1){
            loadUserTweets(1);
        }
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        Intent intent = new Intent();
        long itemID = twitterCursorAdapter.getItemId(position);
        intent.setClass(this, TwitterUserDetailActivity.class);
        intent.putExtra(EXTRA_MESSAGE_ID, itemID);
        startActivity(intent);
    }
}
