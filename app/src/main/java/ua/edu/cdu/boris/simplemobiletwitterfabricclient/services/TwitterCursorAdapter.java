package ua.edu.cdu.boris.simplemobiletwitterfabricclient.services;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import ua.edu.cdu.boris.simplemobiletwitterfabricclient.R;

public class TwitterCursorAdapter extends CursorAdapter {
    public TwitterCursorAdapter(Context context, Cursor cursor, int flags) {
        super(context, cursor, flags);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        View root = LayoutInflater.from(context).inflate(R.layout.tweet_list_item, parent, false);
        ViewHolder holder = new ViewHolder();
        holder.tvMessage = (TextView)root.findViewById(R.id.tvTweetMessage);
        holder.tvDate = (TextView)root.findViewById(R.id.tvCreatedAt);
        root.setTag(holder);
        return root;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        String message = cursor.getString(cursor.getColumnIndexOrThrow("text"));
        String createdAt = cursor.getString(cursor.getColumnIndexOrThrow("created_at"));

        ViewHolder holder = (ViewHolder) view.getTag();
        if(holder != null) {
            holder.tvMessage.setText(message);



            holder.tvDate.setText(convertDate(createdAt));
        }

    }

    public static class ViewHolder {
        public TextView tvMessage;
        public TextView tvDate;
    }
    private String convertDate(String createdAt){
        SimpleDateFormat utcFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss ZZZZZ yyyy", Locale.ENGLISH);
        utcFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

        Date date = null;
        try {
            date = utcFormat.parse(createdAt);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat localFormat = new SimpleDateFormat("dd MMM yyyy HH:mm", Locale.getDefault());
        localFormat.setTimeZone(TimeZone.getDefault());
        return localFormat.format(date);
    }
}
