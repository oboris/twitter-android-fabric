package ua.edu.cdu.boris.simplemobiletwitterfabricclient.dataBase;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import ua.edu.cdu.boris.simplemobiletwitterfabricclient.model.*;

public class DBHelper extends SQLiteOpenHelper {

    private static DBHelper sInstance;

    private static final String DBName = "twittsdb.db3";
    private static final int DBVersion = 10;

    public static synchronized DBHelper getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new DBHelper(context.getApplicationContext());
        }
        return sInstance;
    }
    private DBHelper(Context context){//, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, DBName, null, DBVersion);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SimpleTweet.CreateTableSimpleTweets);
        db.execSQL(SimpleUser.CreateTableSimpleUsers);
     }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + SimpleTweet.TableSimpleTweets);
        db.execSQL("DROP TABLE IF EXISTS " + SimpleUser.TableSimpleUsers);
        onCreate(db);
    }

/////////////////////////Tweets/////////////////////////////////
    public synchronized Cursor getAllTweets() {
        final SQLiteDatabase db = this.getReadableDatabase();
        //final Cursor cursor = db.rawQuery("Select * from " + SimpleTweet.TableSimpleTweets + " ", null);
        final Cursor cursor = db.query(SimpleTweet.TableSimpleTweets, new String[]{"_id", "text", "created_at"}, null, null, null, null, "_id desc");
        if (cursor == null || cursor.isAfterLast()) {
            return null;
        }
        return cursor;
    }

    public synchronized void insertSimpleTweet(SimpleTweet simpleTweet) {
        final SQLiteDatabase db = this.getWritableDatabase();
        db.insertWithOnConflict(SimpleTweet.TableSimpleTweets, null, simpleTweet.getContent(), SQLiteDatabase.CONFLICT_REPLACE);
    }

    public synchronized void deleteAllSimpleTweets() {
        final SQLiteDatabase db = this.getWritableDatabase();
        db.delete(SimpleTweet.TableSimpleTweets, null, null);
    }

    ///////////////////////////Users///////////////////////////////
    public synchronized Cursor getAllUsers() {
        final SQLiteDatabase db = this.getReadableDatabase();
        final Cursor cursor = db.query(SimpleUser.TableSimpleUsers, new String[]{"_id", "name", "screen_name", "profile_image_url"}, null, null, null, null, "_id desc");
        if (cursor == null || cursor.isAfterLast()) {
            return null;
        }
        return cursor;
    }

    public synchronized void insertSimpleUsers(SimpleUser simpleUser) {
        final SQLiteDatabase db = this.getWritableDatabase();
        db.insertWithOnConflict(SimpleUser.TableSimpleUsers, null, simpleUser.getContent(), SQLiteDatabase.CONFLICT_REPLACE);
    }

    public synchronized void deleteAllSimpleUsers() {
        final SQLiteDatabase db = this.getWritableDatabase();
        db.delete(SimpleUser.TableSimpleUsers, null, null);
    }
}
