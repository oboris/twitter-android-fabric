package ua.edu.cdu.boris.simplemobiletwitterfabricclient;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.LinearLayout;

import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.models.Tweet;
import com.twitter.sdk.android.tweetui.TweetUtils;
import com.twitter.sdk.android.tweetui.TweetView;

public class TwitterUserDetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_twitter_user_detail);

        Intent intent = getIntent();
        long tweetItem = intent.getLongExtra(TwitterUserListActivity.EXTRA_MESSAGE_ID, 0);

        final LinearLayout myLayout = (LinearLayout) findViewById(R.id.linlay);

        TweetUtils.loadTweet(tweetItem, new Callback<Tweet>() {
            @Override
            public void success(Result<Tweet> result) {
                assert myLayout != null;
                myLayout.addView(new TweetView(TwitterUserDetailActivity.this, result.data));
            }

            @Override
            public void failure(TwitterException e) {

            }
        });

    }
}
