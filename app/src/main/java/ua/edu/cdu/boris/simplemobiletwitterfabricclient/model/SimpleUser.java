package ua.edu.cdu.boris.simplemobiletwitterfabricclient.model;

import android.content.ContentValues;

public class SimpleUser {

    private long _id;
    private String name;
    private String screenName;
    private String profileImageUrl;

    public SimpleUser(long _id, String name, String screenName, String profileImageUrl) {
        this._id = _id;
        this.name = name;
        this.profileImageUrl = profileImageUrl;
        this.screenName = screenName;
    }

    public long get_id() {
        return _id;
    }

    public String getName() {
        return name;
    }

    public String getScreenName() {
        return screenName;
    }

    public String getProfileImageUrl() {
        return profileImageUrl;
    }

    public static final String TableSimpleUsers ="simpleusers";
    public static final String CreateTableSimpleUsers ="CREATE TABLE IF NOT EXISTS "+  TableSimpleUsers +" (\n" +
            "  _id         integer NOT NULL,\n" +
            "  name        varchar(64) NOT NULL,\n" +
            "  screen_name varchar(64) NOT NULL,\n" +
            "  profile_image_url varchar(256) NOT NULL,\n" +
            "  PRIMARY KEY (_id)\n" +
            ");";

    public ContentValues getContent() {
        final ContentValues values = new ContentValues();
        values.put("_id", get_id());
        values.put("name", getName());
        values.put("screen_name", getScreenName());
        values.put("profile_image_url", getProfileImageUrl());
        return values;
    }
}
