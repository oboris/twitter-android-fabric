package ua.edu.cdu.boris.simplemobiletwitterfabricclient.services;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import ua.edu.cdu.boris.simplemobiletwitterfabricclient.R;

public class TwitterUserCursorAdapter extends CursorAdapter {

    public TwitterUserCursorAdapter(Context context, Cursor c, int flags) {
        super(context, c, flags);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        View root = LayoutInflater.from(context).inflate(R.layout.tweet_user_list_item, parent, false);
        ViewHolder holder = new ViewHolder();

        holder.tvName = (TextView) root.findViewById(R.id.tvUserName1);
        holder.tvScreenName = (TextView) root.findViewById(R.id.tvScreenName1);
        holder.ivTweetImage = (ImageView) root.findViewById(R.id.imageView2);
        holder.ivUserImage = (ImageView) root.findViewById(R.id.ivUserImage1);
        root.setTag(holder);

        return root;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        String name = cursor.getString(cursor.getColumnIndexOrThrow("name"));
        String screenName = cursor.getString(cursor.getColumnIndexOrThrow("screen_name"));
        String profileImageUrl = cursor.getString(cursor.getColumnIndexOrThrow("profile_image_url"));

        ViewHolder holder = (ViewHolder) view.getTag();
        if (holder != null) {
            holder.tvName.setText(name);
            holder.tvScreenName.setText(screenName);
            Picasso.with(context).load(profileImageUrl).into(holder.ivUserImage);
        }
    }

    public static class ViewHolder {
        public TextView tvName;
        public TextView tvScreenName;
        public ImageView ivUserImage;
        public ImageView ivTweetImage;
    }
}
