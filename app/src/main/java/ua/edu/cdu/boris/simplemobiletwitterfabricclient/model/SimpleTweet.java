package ua.edu.cdu.boris.simplemobiletwitterfabricclient.model;

import android.content.ContentValues;

public class SimpleTweet {
    private long _id;
    private String createdAt;
    private String text;

    public SimpleTweet(long _id, String createdAt, String text) {
        this._id = _id;
        this.createdAt = createdAt;
        this.text = text;
    }

    public static final String TableSimpleTweets ="simpletweets";
    public static final String CreateTableSimpleTweets ="CREATE TABLE IF NOT EXISTS "+  TableSimpleTweets +" (\n" +
            "  _id        integer NOT NULL,\n" +
            "  created_at varchar(64) NOT NULL,\n" +
            "  text       varchar(64) NOT NULL,\n" +
            "  PRIMARY KEY (_id)\n" +
            ");";

    public long get_ID() {
        return _id;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public String getText() {
        return text;
    }

    public ContentValues getContent() {
        final ContentValues values = new ContentValues();
        values.put("_id", get_ID());
        values.put("text", getText());
        values.put("created_at", getCreatedAt());
        return values;
    }
}
